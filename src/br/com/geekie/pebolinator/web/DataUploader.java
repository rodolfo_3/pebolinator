package br.com.geekie.pebolinator.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;
import br.com.geekie.pebolinator.models.Match;

/**
 * Deals with server communication.
 * 
 * @author jbarguil
 */
public class DataUploader {

    private static final String HOME_URL = "http://pebostats.herokuapp.com";
    private static final String MATCH_POST_URL = HOME_URL + "/match";
    private static final String CAN_MATCH_START_GET_URL = HOME_URL + "/can_start";

    private static final String TAG = "DataUploader";

    /**
     * Creates a new match. Runs in a separate Thread.
     * 
     * @param match
     * @param callback
     * @return immediately, result is given on callback
     */
    public static void newMatch(Match match, final DataUploaderListener callback) {
        final String json = match.toString();

        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpResponse response = post(MATCH_POST_URL, json);

                // TODO parse response, call callback.onMatchUploadFail() if it's the case.
                callback.onNewMatchCreated();
            }
        }).start();
    }

    /**
     * Posts a match. Runs in a separate Thread.
     * 
     * @param match
     * @param callback
     * @return immediately, result is given on callback
     */
    public static void postMatch(Match match, final DataUploaderListener callback) {
        final String json = match.toString();
        
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpResponse response = post(MATCH_POST_URL, json);

                // TODO parse response, call callback.onMatchUploadFail() if it's the case.
                callback.onMatchUploaded();
            }
        }).start();
    }

    /**
     * Checks if a new match can be started. Runs on a separate Thread.
     * 
     * @param match
     * @param callback
     * @return immediately, result is given on callback
     */
    public static void canMatchStart(Match match, final DataUploaderListener callback) {
        final String url = CAN_MATCH_START_GET_URL + "?" + match.getId();

        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpResponse response = get(url);

                // TODO parse response, call callback.onStartMatchQueryFail() if it's the case.
                callback.onStartMatchQueryCompleted(false);
            }
        });
    }

    private static HttpResponse post(String url, String json) {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        try {
            // Add your data
            List<NameValuePair> data = new ArrayList<NameValuePair>(2);
            data.add(new BasicNameValuePair("data", json));
            httppost.setEntity(new UrlEncodedFormEntity(data));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            Log.i(TAG, response.toString());

            return response;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static HttpResponse get(String url) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(new HttpGet(url));
            Log.i(TAG, response.toString());

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

