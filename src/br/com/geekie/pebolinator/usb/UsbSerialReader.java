package br.com.geekie.pebolinator.usb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.hardware.usb.UsbManager;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

public class UsbSerialReader {

    private UsbManager manager;
    private UsbSerialDriver driver;
    private UsbSerialMessageListener listener;
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();
    private int lastMessageId = -1;
    private ArrayList<Byte> message = new ArrayList<Byte>();
    
    Pattern pGol = Pattern.compile("GOL=[0-9]*");
    Pattern pUid = Pattern.compile("UID=[0-9]*");
	
    public UsbSerialReader(UsbManager manager, UsbSerialMessageListener listener) {
        super();
        this.manager = manager;
        this.listener = listener;
    }
    
    private SerialInputOutputManager mSerialIoManager;
    private final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {

        @Override
        public void onRunError(Exception e) {
            listener.onFail(e.toString());
        }

        @Override
        public void onNewData(final byte[] buffer) {
        	for (byte b : buffer) {
        		message.add(b);
			}
        	
        	String aux = "";
        	for(int i=0; i<message.size(); i++) {
        		Byte v = message.get(i);
        		aux += (char) v.byteValue();
			}
        	
        	int nl = aux.indexOf("\n");
        	if (nl > 0) {
        		try{
        		
        		String[] splited = aux.substring(0, aux.length()-1).split(";");
        		
        		String goal = splited[0];      		
        		String uid = splited[1];
        		
        		splited = goal.split("=");
        		byte goalTeam = Byte.parseByte(splited[1]);
        		
        		splited = uid.split("=");
        		byte uidNumber = Byte.parseByte(splited[1]);
        		callListener(goalTeam, uidNumber);

        		} catch (Exception e){
        			listener.onFail(e.toString());
        		}
        		message.clear();
        	}
        	
        }
        private void callListener(byte team, int uid) {
        		
        	int expectedMessageId = lastMessageId+1; 
        		if (expectedMessageId > uid) {
        			// repeated message
        			listener.onFail("Repeated message: " + Integer.toString(uid));
        			return;
        		}
        		
        		if (expectedMessageId < uid) {
        			listener.onFail("Expecting " +
        							 Integer.toString(expectedMessageId) +
        							 ", received " +
        							 Integer.toString(uid));
        			// I lose something! What can I do?
        			// FIXME
        		}
        		
        		lastMessageId = uid;
        		
        		switch (team) {
        			case 0:
        				listener.onBlackScored();
        				break;
        			case 1:
        				listener.onYellowScored();
        				break;
        			default:
        				listener.onFail("Undefined team:" + Integer.toString(team));
        				return;
        		}
        	
        }
    };

    private void stopIoManager() {
        if (mSerialIoManager != null) {
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

    private void startIoManager() {
    	if (driver != null) {
            mSerialIoManager = new SerialInputOutputManager(driver, mListener);
            mExecutor.submit(mSerialIoManager);
        }
    }

    private void onDeviceStateChange() {
        stopIoManager();
        startIoManager();
    }

    public void start() {
		// Find the first available driver.
		driver = UsbSerialProber.acquire(manager);
		
		if (driver != null) {
			try {
				driver.open();
			    driver.setBaudRate(9600);
			    startIoManager();
			  } catch (IOException e) {
			    // Deal with error.
			  } 
			}
    }
    
    public void stopListening() {
    	try {
    		driver.close();
    	} catch (IOException exception) {
		    // Deal with error.
    	}
    }
}
