package br.com.geekie.pebolinator.usb;

public interface UsbSerialMessageListener {

    /**
     * Called if opening the communication channel with the device fails.
     */
    public void onFailToOpenDevice();

    /**
     * Called when an IOException is caught.
     */
    public void onIOException(Exception exception);

    /**
     * Called when Yellow team scores.
     */
    public void onYellowScored();

    /**
     * Called when Black team scores.
     */
    public void onBlackScored();
    
    /**
     * When received a broken message
     */
    public void onFail(String message);
}
