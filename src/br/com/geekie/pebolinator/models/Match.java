package br.com.geekie.pebolinator.models;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Match {

    private long id;
    private ArrayList<Goal> yellow;
    private ArrayList<Goal> black;

    public Match() {
        super();
        id = System.currentTimeMillis();
        yellow = new ArrayList<Goal>();
        black = new ArrayList<Goal>();
    }

    @Override
    public String toString() {
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        return gson.toJson(this);
    }

    public long getId() {
        return id;
    }

    public boolean yellowScored() {
        return yellow.add(new Goal(System.currentTimeMillis()));
    }

    public boolean blackScored() {
        return black.add(new Goal(System.currentTimeMillis()));
    }
}
