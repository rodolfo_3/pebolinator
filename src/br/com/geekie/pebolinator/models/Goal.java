package br.com.geekie.pebolinator.models;

/**
 * Represents a Goal on the foosball match. Just contains the timestamp of when was scored.
 * 
 * @author jbarguil
 */
public class Goal {
    private long created;

    public Goal(long created) {
        super();
        this.created = created;
    }

}
