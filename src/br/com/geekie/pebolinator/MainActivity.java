package br.com.geekie.pebolinator;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import br.com.geekie.pebolinator.models.Match;
import br.com.geekie.pebolinator.usb.UsbSerialMessageListener;
import br.com.geekie.pebolinator.usb.UsbSerialReader;
import br.com.geekie.pebolinator.web.DataUploader;
import br.com.geekie.pebolinator.web.DataUploaderListener;

@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends Activity {

    private static final String HOME_URL = "http://pebostats.herokuapp.com/";

    private static final String TAG = "MainActivity";
    
    private MyUsbSerialMessageListener usbListener;
    private MyDataUploaderListener webListener;

    private Match currentMatch;

    private UsbSerialReader usbReader;

    private boolean isMatchActive;

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Let's display the progress in the activity title bar, like the
        // browser app does.
        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setUpWebView();

        isMatchActive = false;

        webListener = new MyDataUploaderListener();
        usbListener = new MyUsbSerialMessageListener();

        usbReader = new UsbSerialReader((UsbManager) getSystemService(Context.USB_SERVICE), usbListener);
        usbReader.start();
    }

    private void setUpWebView() {
        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);

        final Activity activity = this;
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000);
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description,
                    String failingUrl) {
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        webView.loadUrl(HOME_URL);

    }

    private void updateWebViewUrl(final String url) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl(url);
            }
        });
    }

    private void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        usbReader.stopListening();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_conjugate:
                startActivity(new Intent(this, ConjugationActivity.class));
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * This method is called when the "New Match" button is clicked. We defined this in the .xml
     * file, with the "onClick" property, in case you're wondering.
     * 
     * @param v
     *            the activated View (in this case, the Button. We don't really need it, but that's
     *            how Android works)
     */
    public void onNewMatchButtonClick(View v) {
        
        // Displays an Alert to make sure the user knows what he's doing.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.are_you_sure);
        builder.setMessage(R.string.this_will_end_current_match);
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Does nothing (the dialog will close) :D
            }
        });

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Incredible! The user is sure! Let's tell the server we need a new match.
                currentMatch = new Match();
                DataUploader.newMatch(currentMatch, webListener);
                isMatchActive = false;
            }
        });

        // Create and shows the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public class MyUsbSerialMessageListener implements UsbSerialMessageListener {
    	public class RunnableToaster implements Runnable {
    		private String textMessage;
    		public RunnableToaster(String message) {
    			this.textMessage = message;
    		}
    		@Override
            public void run() {
                Toast toast = Toast.makeText(MainActivity.this, textMessage, Toast.LENGTH_SHORT);
                toast.show();
            }
    	}

        private void toastMessage(String textMessage) {
            runOnUiThread(new RunnableToaster(textMessage));
        }

        @Override
        public void onYellowScored() {
            Log.i(TAG, "Yellow scored!");
            
            showToast("Yellow +1");

            if (!isMatchActive) {
                Log.i(TAG, "...but match hasn't started, le lol");
                return;
            }

            currentMatch.yellowScored();
            DataUploader.postMatch(currentMatch, webListener);
        }

        @Override
        public void onBlackScored() {
            Log.i(TAG, "Black scored!");
            
            showToast("Black +1");
            
            if (!isMatchActive) {
                Log.i(TAG, "...but match hasn't started, le lol");
                return;
            }

            currentMatch.blackScored();
            DataUploader.postMatch(currentMatch, webListener);
        }

        @Override
        public void onFailToOpenDevice() {
            Log.e(TAG, "Failed to open connection with Arduino!");
            showToast("Arrrrrghhhh!!!! Não foi possível conectar à mesa!!! :'(");
        }

        @Override
        public void onIOException(Exception exception) {
            Log.e(TAG, "IOException caught!");
            showToast("A conexão com a mesa foi perdida!! Meus pêsames pelos gols perdidos, Pelézão.");
        }

        
        @Override
        public void onFail(String message) {
        	Log.e(TAG, "Failed: " + message);
            showToast("Ooooops:" + message);
        }


    }
    
    private class MyDataUploaderListener implements DataUploaderListener {

        private Timer pollingTimer;

        @Override
        public void onNewMatchCreated() {
            Log.i(TAG, "New Match successfully created!");

            // Redirects the user to the Setup match url
            updateWebViewUrl(HOME_URL + "?" + currentMatch.getId());

            // Starts polling if we can start counting the goals
            pollingTimer = new Timer();
            pollingTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    DataUploader.canMatchStart(currentMatch, webListener);
                }
            }, 5000, 1000);
        }

        @Override
        public void onMatchUploaded() {
            // #quibom.
            // Nothing to do here
        }

        @Override
        public void onStartMatchQueryCompleted(boolean canStart) {
            if (canStart) {
                Log.i(TAG, "Match is now starting!");
                isMatchActive = true;
                pollingTimer.cancel();
            }
        }

        @Override
        public void onNewMatchFail() {
            Log.e(TAG, "Match creation failed!");
            showToast("Falha de comunicação com o servidor! :(\nPor favor, tente novamente.");
        }

        @Override
        public void onMatchUploadFail() {
            Log.e(TAG, "Match upload failed!");
            showToast("Falha de comunicação com o servidor!\nTentaremos novamente no próximo gol.");
        }

        @Override
        public void onStartMatchQueryFail() {
            Log.e(TAG, "Start match query failed!");
            showToast("Falha de comunicação com o servidor! Verifique sua conexão com a internet.");
        }

    }

}
